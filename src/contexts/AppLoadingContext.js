import React, { useReducer } from "react";
import { StyleSheet } from "react-native";
import { ActivityIndicator, Portal } from "react-native-paper";
import Spinner from "react-native-loading-spinner-overlay";

// Utilities
import { colors } from "@theme";

const initialState = {
  text: null,
  visible: false,
};

const reducer = (state, action) => {
  switch (action.type) {
    case "close":
      return {
        ...initialState,
        visible: false,
      };
    case "open":
      return {
        ...initialState,
        ...action.payload,
        visible: true,
      };
    default:
      return;
  }
};

const AppLoadingContext = React.createContext({});

const AppLoadingProvider = ({ children }) => {
  const [loading, loadingDispatch] = useReducer(reducer, initialState);

  /**
   * Show the loading indicator
   * @param {string} text    - Loading text
   * @param {Object} options - Customization options
   */
  const startLoading = (text = null, options = {}) =>
    loadingDispatch({ type: "open", payload: { ...options, text } });

  /**
   * Dismiss the loading indicator
   */
  const stopLoading = () => loadingDispatch({ type: "close" });

  const loader = { show: startLoading, hide: stopLoading };

  return (
    <AppLoadingContext.Provider value={{ loader, startLoading, stopLoading }}>
      {children}
      <Portal>
        <Spinner
          animation="fade"
          customIndicator={
            <ActivityIndicator color={colors.white} size="large" />
          }
          overlayColor="rgba(0, 0, 0, 0.5)"
          textContent={loading.text}
          textStyle={styles.text}
          visible={loading.visible}
        />
      </Portal>
    </AppLoadingContext.Provider>
  );
};

const styles = StyleSheet.create({
  text: {
    width: "100%",
    color: colors.white,
    textAlign: "center",
  },
});

export { AppLoadingContext, AppLoadingProvider };
