/**
 * Eliminate nesting multiple context providers to the app!
 *
 * Taken from: https://tobea.dev/post/global-state-cleanliness-with-react-context#lets-get-fancy
 */

import React from "react";

// Utilities
import { AppLoadingProvider, SnackbarProvider } from "@contexts";

/**
 * Compose Context providers together
 * @param {Node}     children  - React children
 * @param {Object[]} providers - React context providers
 */
const ProviderComposer = ({ children, providers }) =>
  providers.reduceRight(
    (kids, parent) =>
      React.cloneElement(parent, {
        children: kids,
      }),
    children,
  );

/**
 * Combine Context providers into single React component
 * @param {Node}     children  - React children
 */
const ContextProvider = ({ children }) => (
  <ProviderComposer providers={[<AppLoadingProvider />, <SnackbarProvider />]}>
    {children}
  </ProviderComposer>
);

export default ContextProvider;
