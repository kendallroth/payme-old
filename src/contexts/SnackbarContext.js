import React, { useReducer } from "react";
import { Snackbar } from "react-native-paper";

// Utilities
import theme from "@theme";

// Delay snackbar slightly to allow previous snackbars to close (animate) properly
const SNACKBAR_DELAY = 200;

const initialState = {
  buttonText: "",
  duration: Snackbar.DURATION_SHORT,
  permanent: false,
  message: "",
  open: false,
  type: "info",
  onDismiss: () => {},
  onPress: () => {},
};

const reducer = (state, action) => {
  switch (action.type) {
    case "close":
      // NOTE: Resetting all state will cause visual bugs while the snackbar closes!
      return {
        ...state,
        open: false,
      };
    case "open":
      return {
        ...initialState,
        ...action.payload,
        open: true,
      };
    default:
      return;
  }
};

const SnackbarContext = React.createContext({});

const SnackbarProvider = ({ children }) => {
  const [snackbar, snackbarDispatch] = useReducer(reducer, initialState);

  let snackbarStyle = {};
  switch (snackbar.type) {
    case "error":
      snackbarStyle = {
        backgroundColor: theme.colors.error,
        color: theme.colors.white,
      };
      break;
    case "info":
    default:
      break;
  }

  const snackbarAction =
    snackbar.buttonText && snackbar.onPress
      ? { label: snackbar.buttonText, onPress: snackbar.onPress }
      : null;

  /**
   * Handle closing the snackbar
   */
  const onDismiss = () => {
    closeNotification();

    if (snackbar.onDismiss) snackbar.onDismiss();
  };

  /**
   * Close the notification
   */
  const closeNotification = () => snackbarDispatch({ type: "close" });

  /**
   * Open a notification
   * @param {string} message - Notifiation message
   * @param {Object} options - Snackbar options
   */
  const notify = (message, options = {}) => {
    // Close the previous notification
    closeNotification();

    // Use short timeout to allow close animation to finish
    setTimeout(() => {
      snackbarDispatch({ type: "open", payload: { ...options, message } });
    }, SNACKBAR_DELAY);
  };

  /**
   * Open an error notification
   * @param {string} message - Notifiation message
   * @param {Object} options - Snackbar options
   */
  const notifyError = (message, options = {}) => {
    notify(message, { ...options, type: "error" });
  };

  return (
    <SnackbarContext.Provider
      value={{ closeNotification, notify, notifyError, snackbar }}
    >
      {children}
      <Snackbar
        action={snackbarAction}
        duration={snackbar.duration}
        style={snackbarStyle}
        visible={snackbar.open}
        onDismiss={onDismiss}
      >
        {snackbar.message}
      </Snackbar>
    </SnackbarContext.Provider>
  );
};

export { SnackbarContext, SnackbarProvider };
