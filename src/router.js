import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

// Screens
import HomeTabs from "@screens/App/router";
import SettingsScreen from "@screens/App/SettingsScreen";

const Stack = createStackNavigator();

const Router = () => (
  <NavigationContainer>
    <Stack.Navigator
      initialRouteName="HomeTabs"
      screenOptions={{ headerShown: false }}
    >
      <Stack.Screen component={HomeTabs} name="HomeTabs" />
      <Stack.Screen component={SettingsScreen} name="Settings" />
    </Stack.Navigator>
  </NavigationContainer>
);

export default Router;
