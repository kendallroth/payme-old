import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

// Screens
import EventsScreen from "./EventsScreen";
import EventDetailsScreen from "./EventDetailsScreen";

const Stack = createStackNavigator();

const EventsStack = () => (
  <Stack.Navigator
    initialRouteName="Events"
    screenOptions={{ headerShown: false }}
  >
    <Stack.Screen component={EventsScreen} name="Events" />
    <Stack.Screen component={EventDetailsScreen} name="EventDetails" />
  </Stack.Navigator>
);

export default EventsStack;
