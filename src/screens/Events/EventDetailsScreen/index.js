import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dimensions, StyleSheet } from "react-native";
import { TabBar, TabView } from "react-native-tab-view";
import { useNavigation, useRoute } from "@react-navigation/native";

// Components
import { ArchiveEventDialog, ManageEventDialog } from "@components/dialogs";
import { AppBar } from "@components/layout";
import EventAttendeesTab from "./EventAttendeesTab";
import EventPeopleTab from "./EventPeopleTab";

// Utilities
import { useSnackbar } from "@hooks";
import { archiveEvent, selectEvent, updateEvent } from "@store/slices/events";
import theme from "@theme";
import { formatDateString } from "@utilities/dates";

const initialLayout = { width: Dimensions.get("window").width };

const EventDetailsScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const dispatch = useDispatch();
  const { notify } = useSnackbar();

  const event = useSelector((state) => selectEvent(state, route.params.id));

  const [isEditDialogShown, setIsEditDialogShown] = useState(false);
  const [isArchiveDialogShown, setIsArchiveDialogShown] = useState(false);
  const isArchiving = !event?.archivedAt;

  const [index, setIndex] = useState(0);
  const [routes] = useState([
    { key: "attendees", title: "Attendees" },
    { key: "people", title: "People" },
  ]);

  // NOTE: Rare edge case if event is not found or deleted...
  if (!event) {
    if (navigation.isFocused()) {
      navigation.popToTop();
    }
    return null;
  }

  const renderScene = ({ route: r }) => {
    switch (r.key) {
      case "attendees":
        return <EventAttendeesTab event={event} />;
      case "people":
        return <EventPeopleTab event={event} />;
      default:
        return null;
    }
  };
  const renderTabBar = (props) => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: theme.colors.accent }}
    />
  );

  /**
   * Cancel archiving a event
   */
  const onArchiveEventCancel = () => {
    setIsArchiveDialogShown(false);
  };

  /**
   * Confirm archiving a event
   */
  const onArchiveEventConfirm = () => {
    dispatch(archiveEvent(event));

    setIsArchiveDialogShown(false);

    isArchiving && navigation.goBack();
    notify(`${isArchiving ? "Archived" : "Unarchived"} ${event.name}`);
  };

  /**
   * Open the archive event dialog
   */
  const onArchiveEventPress = () => {
    setIsArchiveDialogShown(true);
  };

  /**
   * Open the edit event dialog
   */
  const onEditEventPress = () => {
    setIsEditDialogShown(true);
  };

  /**
   * Cancel editing a event
   */
  const onEditEventCancel = () => {
    setIsEditDialogShown(false);
  };

  /**
   * Confirm editing a event
   * @param {Object} updatedEvent - Updated event
   */
  const onEditEventConfirm = (updatedEvent) => {
    dispatch(updateEvent(updatedEvent));

    setIsEditDialogShown(false);

    notify(`Updated ${updatedEvent.name}`);
  };

  // if (!event) return null;

  return (
    <>
      <AppBar
        subtitle={`${formatDateString(event?.date)}${
          event.archivedAt ? " - Archived" : ""
        }`}
        title={event.name}
      >
        <AppBar.Action
          disabled={event.archivedAt}
          icon="pencil"
          onPress={onEditEventPress}
        />
        <AppBar.Action
          icon={event.archivedAt ? "package-up" : "package-down"}
          onPress={onArchiveEventPress}
        />
      </AppBar>
      <ArchiveEventDialog
        event={event}
        visible={isArchiveDialogShown}
        onCancel={onArchiveEventCancel}
        onConfirm={onArchiveEventConfirm}
      />
      <ManageEventDialog
        event={event}
        visible={isEditDialogShown}
        onCancel={onEditEventCancel}
        onConfirm={onEditEventConfirm}
      />
      <TabView
        initialLayout={initialLayout}
        navigationState={{ index, routes }}
        renderScene={renderScene}
        renderTabBar={renderTabBar}
        onIndexChange={setIndex}
      />
    </>
  );
};

// eslint-disable-next-line no-unused-vars
const styles = StyleSheet.create({});

export default EventDetailsScreen;
