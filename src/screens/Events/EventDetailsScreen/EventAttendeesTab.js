import React, { useState } from "react";
import { StyleSheet, Vibration, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { Text } from "react-native-paper";

// Components
import { ConfirmDialog } from "@components/dialogs";
import AttendeeList from "./EventAttendeesList";

// Utilities
import { useSnackbar } from "@hooks";
import {
  selectEventAttendees,
  toggleAttendance,
  toggleAttendancePaid,
} from "@store/slices/attendance";

const EventAttendeesTab = (props) => {
  const { event } = props;

  const dispatch = useDispatch();
  const { notify } = useSnackbar();

  const attendees = useSelector((state) =>
    selectEventAttendees(state, event.id),
  );

  const [removedAttendee, setRemovedAttendee] = useState(null);

  const hasAttendeeRemoval = Boolean(removedAttendee);

  /**
   * Update whether an attendee has paid
   * @param {Object} attendee - Attendee object
   */
  const onPayPress = (attendee) => {
    Vibration.vibrate(100);

    dispatch(
      toggleAttendancePaid({
        eventId: event.id,
        paid: !attendee.paidAt,
        personId: attendee.id,
      }),
    );
  };

  /**
   * Cancel removing an attendee
   */
  const onRemoveAttendeeCancel = () => {
    setRemovedAttendee(null);
  };

  /**
   * Confirm removing an attendee
   */
  const onRemoveAttendeeConfirm = () => {
    dispatch(
      toggleAttendance({
        attending: false,
        eventId: event.id,
        personId: removedAttendee.id,
      }),
    );

    setRemovedAttendee(null);

    notify(`Removed ${removedAttendee.name}`);
  };

  /**
   * Open the remove attendee dialog
   * @param {object} attendee - Removed attendee ID
   */
  const onRemoveAttendeePress = (attendee) => {
    setRemovedAttendee(attendee);
  };

  return (
    <View style={styles.tab}>
      <ConfirmDialog
        title="Remove Attendee"
        visible={hasAttendeeRemoval}
        onCancel={onRemoveAttendeeCancel}
        onConfirm={onRemoveAttendeeConfirm}
      >
        <Text>Are you sure you want to remove this attendee?</Text>
        <Text style={styles.removeDialogAttendee}>{removedAttendee?.name}</Text>
      </ConfirmDialog>
      <AttendeeList
        attendees={attendees}
        onPay={onPayPress}
        onRemove={onRemoveAttendeePress}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  removeDialogAttendee: {
    marginTop: 16,
    fontWeight: "bold",
  },
  tab: {
    flex: 1,
    alignItems: "center",
  },
});

export default EventAttendeesTab;
