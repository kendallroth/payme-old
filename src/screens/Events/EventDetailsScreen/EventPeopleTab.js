import React from "react";
import { StyleSheet, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";

// Components
import PeopleChecklist from "./PeopleChecklist";

// Utilities
import { toggleAttendance } from "@store/slices/attendance";
import { selectPeopleWithAttendanceForEvent } from "@store/slices/people";

const EventPeopleTab = (props) => {
  const { event } = props;

  const dispatch = useDispatch();
  const peopleWithAttendance = useSelector((state) =>
    selectPeopleWithAttendanceForEvent(state, event.id),
  );

  /**
   * Update whether a person is attending an event
   * @param {string}  personId  - Toggled person ID
   * @param {boolean} attending - Whether a person is attending
   */
  const togglePerson = (personId, attending) => {
    dispatch(
      toggleAttendance({ attending, eventId: event.id, personId: personId }),
    );
  };

  return (
    <View style={styles.tab}>
      <PeopleChecklist people={peopleWithAttendance} onCheck={togglePerson} />
    </View>
  );
};

const styles = StyleSheet.create({
  tab: {
    flex: 1,
    alignItems: "center",
  },
});

export default EventPeopleTab;
