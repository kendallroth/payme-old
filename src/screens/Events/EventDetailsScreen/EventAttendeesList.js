import React, { useState } from "react";
import { FlatList, StyleSheet, View } from "react-native";
import { IconButton, List } from "react-native-paper";

// Components
import FilterBar from "@components/FilterBar";
import { EmptyView } from "@components/layout";
import UnpaidIndicator from "@components/UnpaidIndicator";

// Utilities
import theme from "@theme";
import { formatDateString } from "@utilities/dates";

const AttendeeList = (props) => {
  const { attendees, onPay, onRemove } = props;

  const [listFilter, setListFilter] = useState("all");

  const filteredAttendees =
    listFilter === "all" ? attendees : attendees.filter((a) => !a.paidAt);

  const renderEmptyComponent = () => {
    return attendees.length ? (
      <EmptyView
        image={require("@assets/images/undraw_celebration_blue.png")}
        text="Looks like all attendees have paid!"
      />
    ) : (
      <EmptyView
        image={require("@assets/images/undraw_not_found_blue.png")}
        text="Looks like there are no attendees yet!"
      />
    );
  };

  const renderHeaderComponent = () => (
    <FilterBar
      filters={[
        { color: theme.colors.greyLight, text: "All", value: "all" },
        { color: theme.colors.error, text: "Unpaid", value: "unpaid" },
      ]}
      style={{ marginTop: 16, marginBottom: 8 }}
      value={listFilter}
      onSelect={(filter) => setListFilter(filter)}
    />
  );

  const renderPersonItem = ({ item }) => (
    <List.Item
      key={item.id}
      description={item.paidAt ? formatDateString(item.paidAt) : "Unpaid"}
      descriptionStyle={styles.listItemDescription}
      right={() => renderPersonItemRight(item)}
      title={item.name}
      titleStyle={!item.paidAt && styles.listItemTitleUnpaid}
    />
  );

  const renderPersonItemRight = (item) => (
    <View style={theme.styles.listItemActions}>
      <UnpaidIndicator unpaid={!item.paidAt} />
      {onPay && (
        <IconButton
          icon={item.paidAt ? "currency-usd" : "currency-usd-off"}
          // NOTE: Unsure why empty onPress is apparently necessary?
          onPress={() => {}}
          onLongPress={() => onPay(item)}
        />
      )}
      {onRemove && (
        <IconButton
          disabled={item.paidAt}
          icon="delete"
          onPress={() => onRemove(item)}
        />
      )}
    </View>
  );

  return (
    <FlatList
      data={filteredAttendees}
      keyExtractor={(item) => item.id}
      ListEmptyComponent={renderEmptyComponent}
      ListHeaderComponent={renderHeaderComponent}
      renderItem={renderPersonItem}
      style={theme.styles.list}
    />
  );
};

const styles = StyleSheet.create({
  listItemDescription: {
    fontStyle: "italic",
  },
  listItemTitleUnpaid: {
    color: theme.colors.error,
    fontWeight: "bold",
  },
});

export default AttendeeList;
