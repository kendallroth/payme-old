import React from "react";
import { FlatList, StyleSheet } from "react-native";
import { Checkbox, List } from "react-native-paper";

// Components
import { EmptyView } from "@components/layout";
import UnpaidIndicator from "@components/UnpaidIndicator";

// Utilities
import theme from "@theme";

/**
 * Checklist of people for Event
 * @param {Object[]} people  - List of people (with attendance info)
 * @param {Function} onCheck - Person check handler
 */
const PeopleCheckList = (props) => {
  const { people, onCheck } = props;

  /**
   * Customize check handler (paid attendees cannot be removed!)
   * @param   {Object}   item - Person list item
   * @returns {function} Function to handle selection
   */
  const getOnCheckHandler = (item) => {
    // Paid attendees cannot be removed!
    if (item.attending && item.paidAt) return null;

    return () => onCheck(item.id, !item.attending);
  };

  /**
   * Render a person item
   * @param   {Object} data.item - List item
   * @returns {Node}   Rendered node
   */
  const renderPersonItem = ({ item }) => (
    <List.Item
      key={item.id}
      left={() => (
        <Checkbox
          color={theme.colors.primary}
          disabled={item.attending && item.paidAt}
          status={item.attending ? "checked" : "unchecked"}
          onPress={getOnCheckHandler(item)}
        />
      )}
      right={() => item.attending && <UnpaidIndicator unpaid={!item.paidAt} />}
      title={item.name}
      onPress={getOnCheckHandler(item)}
    />
  );

  return (
    <FlatList
      data={people}
      keyExtractor={(item) => item.id}
      ListEmptyComponent={() => (
        <EmptyView
          image={require("@assets/images/undraw_not_found_blue.png")}
          text="Looks like no people have been added yet!"
        />
      )}
      renderItem={renderPersonItem}
      style={theme.styles.list}
    />
  );
};

// eslint-disable-next-line no-unused-vars
const styles = StyleSheet.create({});

export default PeopleCheckList;
