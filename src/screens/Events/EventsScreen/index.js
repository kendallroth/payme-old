import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { StyleSheet } from "react-native";
import { List, Switch } from "react-native-paper";

// Components
import { ArchiveEventDialog, ManageEventDialog } from "@components/dialogs";
import { AppBar } from "@components/layout";
import PortalFAB from "@components/PortalFAB";
import EventsList from "./EventsList";

// Utilities
import { useSnackbar } from "@hooks";
import {
  addEvent,
  archiveEvent,
  selectEventsWithAttendance,
  updateEvent,
} from "@store/slices/events";

const EventsScreen = () => {
  const [isManageEventDialogShown, setIsManageEventDialogShown] = useState(
    false,
  );
  const [isShowingArchived, setIsShowingArchived] = useState(false);
  const [editedEvent, setEditedEvent] = useState(null);
  const [archivedEvent, setArchivedEvent] = useState(null);
  const dispatch = useDispatch();
  const events = useSelector(selectEventsWithAttendance);

  const { notify } = useSnackbar();

  const unarchivedEvents = events.filter((p) => !p.archivedAt);
  const filteredEvents = isShowingArchived ? events : unarchivedEvents;
  const isArchiving = !archivedEvent?.archivedAt;
  const hasDeletion = Boolean(archivedEvent);

  /**
   * Open the add event dialog
   */
  const onAddEventPress = () => {
    setIsManageEventDialogShown(true);
  };

  /**
   * Cancel archiving a event
   */
  const onArchiveEventCancel = () => {
    setArchivedEvent(null);
  };

  /**
   * Confirm archiving a event
   */
  const onArchiveEventConfirm = () => {
    dispatch(archiveEvent(archivedEvent));

    setArchivedEvent(null);

    notify(`${isArchiving ? "Archived" : "Unarchived"} ${archivedEvent.name}`);
  };

  /**
   * Open the archive event dialog
   * @param {Object} event - Archived event
   */
  const onArchiveEventPress = (event) => {
    setArchivedEvent(event);
  };

  /**
   * Open the edit event dialog
   * @param {Object} event - Edited event
   */
  const onEditEventPress = (event) => {
    setEditedEvent(event);
    setIsManageEventDialogShown(true);
  };

  /**
   * Cancel adding/editing a event
   */
  const onManageEventCancel = () => {
    setEditedEvent(null);
    setIsManageEventDialogShown(false);
  };

  /**
   * Confirm adding/editing a event
   * @param {Object} event - New event
   */
  const onManageEventConfirm = (event) => {
    const wasEditing = Boolean(editedEvent);

    if (wasEditing) {
      dispatch(updateEvent(event));

      notify(`Updated ${event.name}`);
    } else {
      dispatch(addEvent(event));

      notify(`Added ${event.name}`);
    }

    setEditedEvent(null);
    setIsManageEventDialogShown(false);
  };

  return (
    <>
      <AppBar goBack={false} logo title="Events">
        <AppBar.Action disabled icon="magnify" onPress={() => {}} />
        <AppBar.MenuAction>
          <List.Subheader>Filter Events</List.Subheader>
          <List.Item
            right={() => (
              <Switch
                value={isShowingArchived}
                onValueChange={() => setIsShowingArchived(!isShowingArchived)}
              />
            )}
            title="Show archived events"
          />
        </AppBar.MenuAction>
      </AppBar>
      {/* NOTE: Portal FAB must be rendered before other portals to properly place FAB beneath */}
      <PortalFAB icon="plus" onPress={onAddEventPress} />
      <ArchiveEventDialog
        event={archivedEvent}
        visible={hasDeletion}
        onCancel={onArchiveEventCancel}
        onConfirm={onArchiveEventConfirm}
      />
      <ManageEventDialog
        event={editedEvent}
        visible={isManageEventDialogShown}
        onCancel={onManageEventCancel}
        onConfirm={onManageEventConfirm}
      />
      <EventsList
        events={filteredEvents}
        onArchive={onArchiveEventPress}
        onEdit={onEditEventPress}
      />
    </>
  );
};

// eslint-disable-next-line no-unused-vars
const styles = StyleSheet.create({});

export default EventsScreen;
