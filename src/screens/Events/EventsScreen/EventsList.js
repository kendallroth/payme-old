import React from "react";
import { FlatList, StyleSheet, View } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { IconButton, List } from "react-native-paper";

// Components
import UnpaidIndicator from "@components/UnpaidIndicator";
import { EmptyView } from "@components/layout";

// Utilities
import { formatDateString } from "@utilities/dates";
import theme from "@theme";

const EventsScreen = (props) => {
  const { events, onArchive, onEdit } = props;

  const navigation = useNavigation();

  const renderEventItem = ({ item }) => (
    <List.Item
      key={item.id}
      description={formatDateString(item.date)}
      descriptionStyle={styles.listItemDescription}
      right={() => renderEventItemRight(item)}
      title={item.name}
      titleStyle={[item.archivedAt ? styles.listItemTextArchived : null]}
      onPress={() => navigation.navigate("EventDetails", { id: item.id })}
    />
  );

  const renderEventItemRight = (item) => (
    <View style={theme.styles.listItemActions}>
      <UnpaidIndicator unpaid={item.unpaid} />
      <IconButton
        disabled={item.archivedAt}
        icon="account-multiple"
        onPress={() => navigation.navigate("EventDetails", { id: item.id })}
      />
      {onEdit && (
        <IconButton
          disabled={item.archivedAt}
          icon="pencil"
          onPress={() => onEdit(item)}
        />
      )}
      {onArchive && (
        <IconButton
          icon={item.archivedAt ? "package-up" : "package-down"}
          onPress={() => onArchive(item)}
        />
      )}
    </View>
  );

  return (
    <FlatList
      data={events}
      ListEmptyComponent={() => (
        <EmptyView
          image={require("@assets/images/undraw_not_found_blue.png")}
          text="Looks like no events have been added yet!"
        />
      )}
      keyExtractor={(item) => item.id}
      renderItem={renderEventItem}
      style={theme.styles.list}
    />
  );
};

const styles = StyleSheet.create({
  listItemDescription: {
    fontStyle: "italic",
  },
  listItemTextArchived: {
    color: theme.colors.grey,
    fontStyle: "italic",
  },
});

export default EventsScreen;
