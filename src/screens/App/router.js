import React from "react";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { useSelector } from "react-redux";

// Screens
import EventsStack from "@screens/Events/router";
import HomeScreen from "@screens/App/HomeScreen";
import PeopleStack from "@screens/People/router";

// Utilities
import { selectUnpaidEventCount } from "@store/slices/events";
import { selectUnpaidPeopleCount } from "@store/slices/people";

const Tab = createMaterialBottomTabNavigator();

const AppTabStack = () => {
  const unpaidEventsCount = useSelector(selectUnpaidEventCount);
  const unpaidPeopleCount = useSelector(selectUnpaidPeopleCount);

  return (
    <Tab.Navigator backBehavior="history" initialRouteName="Home" shifting>
      <Tab.Screen
        component={HomeScreen}
        name="Home"
        options={{ tabBarIcon: "home" }}
      />
      <Tab.Screen
        component={EventsStack}
        name="EventsStack"
        options={{
          tabBarBadge: unpaidEventsCount || null,
          tabBarIcon: "calendar",
          tabBarLabel: "Events",
        }}
      />
      <Tab.Screen
        component={PeopleStack}
        name="PeopleStack"
        options={{
          tabBarBadge: unpaidPeopleCount || null,
          tabBarIcon: "account-multiple",
          tabBarLabel: "People",
        }}
      />
    </Tab.Navigator>
  );
};

export default AppTabStack;
