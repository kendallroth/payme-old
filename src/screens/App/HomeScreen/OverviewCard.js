import React from "react";
import PropTypes from "prop-types";
import { useNavigation } from "@react-navigation/native";
import { StyleSheet } from "react-native";
import { Avatar, Card, IconButton } from "react-native-paper";

// Components
import { EmptyMessage } from "@components/layout";

// Utilities
import theme from "@theme";

/**
 * Overview card
 * @param {string}   emptyText   - Empty list text
 * @param {string}   icon        - Title icon
 * @param {Object[]} items       - Overview items
 * @param {Function} renderItem  - Render list item
 * @param {Object}   style       - Style object
 * @param {string}   title       - Title
 * @param {string}   viewRoute   - Overview link route
 */
const OverviewCard = ({
  emptyText,
  icon,
  items,
  renderItem,
  style,
  title,
  viewRoute,
}) => {
  const navigation = useNavigation();

  return (
    <Card elevation={4} style={{ ...styles.card, ...style }}>
      <Card.Title
        left={(props) => (
          <Avatar.Icon
            {...props}
            icon={icon}
            style={{ backgroundColor: "transparent" }}
          />
        )}
        leftStyle={styles.cardTitleIcon}
        style={styles.cardTitle}
        title={title}
        titleStyle={styles.cardTitleText}
        right={(props) => [
          <IconButton
            {...props}
            key="viewItems"
            color={theme.colors.white}
            icon="magnify"
            onPress={() => {
              navigation.navigate(viewRoute);
            }}
          />,
        ]}
        rightStyle={styles.cardIcons}
      />
      <Card.Content style={styles.cardContent}>
        {items.map((item) => renderItem(item))}
        {!items.length && (
          <EmptyMessage
            text={emptyText || "No unpaid items"}
            visible={!items.length}
          />
        )}
      </Card.Content>
    </Card>
  );
};

OverviewCard.propTypes = {
  emptyText: PropTypes.string,
  icon: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  renderItem: PropTypes.func.isRequired,
  style: PropTypes.object,
  title: PropTypes.string.isRequired,
  viewRoute: PropTypes.string.isRequired,
};

OverviewCard.defaultProps = {
  emptyText: null,
  style: {},
};

const styles = StyleSheet.create({
  card: {
    width: "100%",
    overflow: "hidden",
  },
  cardContent: {
    paddingLeft: 0,
    paddingRight: 0,
    paddingBottom: 4,
  },
  cardIcons: {
    flexDirection: "row",
  },
  cardTitle: {
    minHeight: 64,
    backgroundColor: theme.colors.primary,
  },
  cardTitleIcon: {
    marginRight: 8,
  },
  cardTitleText: {
    color: theme.colors.white,
  },
});

export default OverviewCard;
