import React, { Fragment, useMemo } from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import { useSelector } from "react-redux";
import sort from "fast-sort";
import { List } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";

// Components
import { AppBar, EmptyView } from "@components/layout";
import UnpaidIndicator from "@components/UnpaidIndicator";
import OverviewCard from "./OverviewCard";

// Utilities
import { selectEventsWithAttendance } from "@store/slices/events";
import { selectPeopleWithAttendance } from "@store/slices/people";
import { formatDateString } from "@utilities/dates";

// Get the top 3 unpaid events
const getTopUnpaidEvents = (events) => {
  const eventsWithAttendance = events.filter((e) => e.unpaid);
  const topUnpaidEvents = sort(eventsWithAttendance).desc("unpaid").slice(0, 3);
  return sort(topUnpaidEvents).desc("date");
};

// Get the top 3 unpaid people
const getTopUnpaidPeople = (people) => {
  const peopleWithAttendance = people.filter((p) => p.unpaid);
  const topUnpaidPeople = sort(peopleWithAttendance).desc("unpaid").slice(0, 3);
  return sort(topUnpaidPeople).asc("name");
};

const HomeScreen = () => {
  const navigation = useNavigation();

  const eventsWithAttendance = useSelector(selectEventsWithAttendance);
  const peopleWithAttendance = useSelector(selectPeopleWithAttendance);

  // Memoize top unpaid entities (no selector since it is only on this screen)
  // NOTE: This is technically unnecessary since Home should never rerender...
  const topUnpaidEvents = useMemo(
    () => getTopUnpaidEvents(eventsWithAttendance),
    [eventsWithAttendance],
  );
  const topUnpaidPeople = useMemo(
    () => getTopUnpaidPeople(peopleWithAttendance),
    [peopleWithAttendance],
  );

  const onEventPress = (event) => {
    navigation.navigate("HomeTabs", {
      screen: "EventsStack",
      params: {
        params: { id: event.id },
        screen: "EventDetails",
      },
    });
  };

  const onPersonPress = (person) => {
    navigation.navigate("HomeTabs", {
      screen: "PeopleStack",
      params: {
        params: { id: person.id },
        screen: "PersonDetails",
      },
    });
  };

  const onSettingsPress = () => {
    navigation.navigate("Settings");
  };

  const hasCelebration = !topUnpaidEvents.length && !topUnpaidPeople.length;

  return (
    <Fragment>
      <AppBar goBack={false} logo title="PayMe">
        <AppBar.Action icon="settings" onPress={onSettingsPress} />
      </AppBar>
      <ScrollView contentContainerStyle={styles.screen}>
        <View style={styles.overviewCards}>
          <OverviewCard
            emptyText="No events with unpaid attendees!"
            icon="calendar"
            items={topUnpaidEvents}
            renderItem={(event) => (
              <List.Item
                key={event.name}
                description={formatDateString(event.date)}
                descriptionStyle={styles.listItemDescription}
                right={(props) => (
                  <UnpaidIndicator iconProps={props} unpaid={event.unpaid} />
                )}
                style={styles.listItem}
                title={event.name}
                titleStyle={styles.listItemTitle}
                onPress={() => onEventPress(event)}
              />
            )}
            title="Events"
            style={{ marginBottom: 24 }}
            viewRoute="Events"
          />
          <OverviewCard
            emptyText="No people with unpaid events!"
            icon="account-multiple"
            items={topUnpaidPeople}
            renderItem={(person) => (
              <List.Item
                key={person.name}
                right={(props) => (
                  <UnpaidIndicator iconProps={props} unpaid={person.unpaid} />
                )}
                style={styles.listItem}
                title={person.name}
                titleStyle={styles.listItemTitle}
                onPress={() => onPersonPress(person)}
              />
            )}
            title="People"
            viewRoute="People"
          />
        </View>
        {hasCelebration && (
          <EmptyView
            image={require("@assets/images/undraw_celebration_blue.png")}
            style={{ marginTop: 0 }}
            text="Looks like everything is paid up!"
            textStyle={{ marginTop: 8 }}
          />
        )}
      </ScrollView>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  headerContent: {
    marginLeft: 0,
  },
  overviewCards: {
    alignItems: "center",
    width: "100%",
    padding: 24,
  },
  overviewListItem: {
    flexDirection: "row",
    alignItems: "center",
    minHeight: 48,
    paddingTop: 2,
    paddingBottom: 2,
  },
  overviewListItemTitle: {
    fontWeight: "bold",
  },
  overviewListItemDescription: {
    fontStyle: "italic",
  },
  screen: {
    alignItems: "center",
  },
});

export default HomeScreen;
