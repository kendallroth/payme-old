import React from "react";
import { Alert, ScrollView, StyleSheet } from "react-native";
import { useDispatch } from "react-redux";
import { Divider, List } from "react-native-paper";

// Components
import { AppBar, EmptyView } from "@components/layout";

// Utilities
import { useAppLoading, useSnackbar } from "@hooks";
import { addDebugData, resetApp } from "@store/slices/settings";

const SettingsScreen = () => {
  const dispatch = useDispatch();
  const { loader } = useAppLoading();
  const { notify } = useSnackbar();

  /**
   * Reset the app state
   */
  const onAppResetPress = () => {
    Alert.alert(
      "Reset App",
      "This will reset the app state and is irreversible!",
      [
        { text: "Cancel", style: "cancel" },
        {
          text: "Confirm",
          onPress: async () => {
            loader.show("Resetting app...");
            await dispatch(resetApp());

            loader.hide();
            notify("App has been reset");
          },
        },
      ],
    );
  };

  /**
   * Add sample test data
   */
  const onAddDebugDataPress = () => {
    Alert.alert(
      "Add Test Data",
      "This will add sample test data into the app state!",
      [
        { text: "Cancel", style: "cancel" },
        {
          text: "Confirm",
          onPress: async () => {
            loader.show("Adding test data...");
            await dispatch(addDebugData());

            loader.hide();
            notify("Test data has been added");
          },
        },
      ],
    );
  };

  return (
    <ScrollView>
      <AppBar title="Settings" />
      <List.Section>
        <List.Subheader>Developer Settings</List.Subheader>
        <List.Item
          description="Press and hold to reset app"
          left={(props) => <List.Icon {...props} icon="lock-reset" />}
          title="Reset App"
          onPress={() => {}}
          onLongPress={onAppResetPress}
        />
        <List.Item
          description="Press and hold to add sample test data"
          left={(props) => <List.Icon {...props} icon="database-plus" />}
          title="Add Test Data"
          onPress={() => {}}
          onLongPress={onAddDebugDataPress}
        />
      </List.Section>
      <Divider />
      <EmptyView
        image={require("@assets/images/undraw_under_construction_blue.png")}
        text="Under Construction"
      />
    </ScrollView>
  );
};

// eslint-disable-next-line no-unused-vars
const styles = StyleSheet.create({});

export default SettingsScreen;
