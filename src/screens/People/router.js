import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

// Screens
import PeopleScreen from "./PeopleScreen";
import PersonDetailsScreen from "./PersonDetailsScreen";

const Stack = createStackNavigator();

const PeopleStack = () => (
  <Stack.Navigator
    initialRouteName="People"
    screenOptions={{ headerShown: false }}
  >
    <Stack.Screen component={PeopleScreen} name="People" />
    <Stack.Screen component={PersonDetailsScreen} name="PersonDetails" />
  </Stack.Navigator>
);

export default PeopleStack;
