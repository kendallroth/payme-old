import React from "react";
import { FlatList, StyleSheet, View } from "react-native";
import { IconButton, List } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";

// Components
import UnpaidIndicator from "@components/UnpaidIndicator";
import { EmptyView } from "@components/layout";
import theme from "@theme";

const PeopleList = (props) => {
  const { people, onArchive, onEdit } = props;

  const navigation = useNavigation();

  const renderPersonItem = ({ item }) => (
    <List.Item
      key={item.id}
      right={() => renderPersonItemRight(item)}
      title={item.name}
      titleStyle={[item.archivedAt ? styles.listItemTextArchived : null]}
      onPress={() => navigation.navigate("PersonDetails", { id: item.id })}
    />
  );

  const renderPersonItemRight = (item) => (
    <View style={theme.styles.listItemActions}>
      <UnpaidIndicator unpaid={item.unpaid} />
      {onEdit && (
        <IconButton
          disabled={item.archivedAt}
          icon="pencil"
          onPress={() => onEdit(item)}
        />
      )}
      {onArchive && (
        <IconButton
          icon={item.archivedAt ? "package-up" : "package-down"}
          onPress={() => onArchive(item)}
        />
      )}
    </View>
  );

  return (
    <FlatList
      data={people}
      keyExtractor={(item) => item.id}
      ListEmptyComponent={() => (
        <EmptyView
          image={require("@assets/images/undraw_not_found_blue.png")}
          text="Looks like no people have been added yet!"
        />
      )}
      renderItem={renderPersonItem}
      style={theme.styles.list}
    />
  );
};

const styles = StyleSheet.create({
  listItemTextArchived: {
    color: theme.colors.grey,
    fontStyle: "italic",
  },
});

export default PeopleList;
