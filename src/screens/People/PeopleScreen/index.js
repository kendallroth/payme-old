import React, { useState } from "react";
import { StyleSheet } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { List, Switch } from "react-native-paper";
import { useFocusEffect, useIsFocused } from "@react-navigation/native";

// Components
import { ArchivePersonDialog, ManagePersonDialog } from "@components/dialogs";
import { AppBar } from "@components/layout";
import PortalFAB from "@components/PortalFAB";
import PeopleList from "./PeopleList";

// Utilities
import { useSnackbar } from "@hooks";
import {
  addPerson,
  archivePerson,
  selectPeopleWithAttendance,
  updatePerson,
} from "@store/slices/people";

const PeopleScreen = () => {
  const [isFabOpen, setIsFabOpen] = useState(false);
  const [isShowingArchived, setIsShowingArchived] = useState(false);
  const [addPersonMode, setAddPersonMode] = useState("single");
  const [isManagePersonDialogShown, setIsManagePersonDialogShown] = useState(
    false,
  );
  const [archivedPerson, setArchivedPerson] = useState(null);
  const [editedPerson, setEditedPerson] = useState(null);

  const dispatch = useDispatch();
  const people = useSelector(selectPeopleWithAttendance);
  const { notify } = useSnackbar();

  // NOTE: Because FAB uses a portal it must only display when screen is focused
  const isPeopleFabShown = useIsFocused();
  // NOTE: Close the FAB menu when changing screens
  useFocusEffect(React.useCallback(() => () => setIsFabOpen(false), []));

  const isArchiving = !archivedPerson?.archivedAt;
  const unarchivedPeople = people.filter((p) => !p.archivedAt);
  const filteredPeople = isShowingArchived ? people : unarchivedPeople;

  const fabActions = [
    {
      icon: "account-plus",
      label: "Add Person",
      onPress: () => onAddPersonPress(false),
    },
    {
      icon: "account-multiple-plus",
      label: "Add People",
      onPress: () => onAddPersonPress(true),
    },
  ];

  const hasArchival = Boolean(archivedPerson);

  /**
   * Open the add person dialog
   * @param {boolean} isMultiple - Whether adding multiple people
   */
  const onAddPersonPress = (isMultiple = false) => {
    setIsManagePersonDialogShown(true);
    setAddPersonMode(isMultiple ? "multiple" : "single");
  };

  /**
   * Cancel archiving a person
   */
  const onArchivePersonCancel = () => {
    setArchivedPerson(null);
  };

  /**
   * Confirm archiving a person
   */
  const onArchivePersonConfirm = () => {
    dispatch(archivePerson(archivedPerson));

    setArchivedPerson(null);

    notify(`${isArchiving ? "Archived" : "Unarchived"} ${archivedPerson.name}`);
  };

  /**
   * Open the toogle archive person dialog
   * @param {Object} person - Selected person
   */
  const onArchivePersonPress = (person) => {
    setArchivedPerson(person);
  };

  /**
   * Open the edit person dialog
   * @param {Object} person - Edited person
   */
  const onEditPersonPress = (person) => {
    setEditedPerson(person);
    setIsManagePersonDialogShown(true);
  };

  /**
   * Cancel adding/editing a person
   */
  const onManagePersonCancel = () => {
    setEditedPerson(null);
    setIsManagePersonDialogShown(false);
  };

  /**
   * Confirm adding/editing a person
   * @param {Object}  person      - New person
   * @param {boolean} shouldClose - Whether dialog should close after adding (ie. adding another)
   */
  const onManagePersonConfirm = (person, shouldClose = true) => {
    const wasEditing = Boolean(editedPerson);

    if (wasEditing) {
      dispatch(updatePerson(person));

      notify(`Updated ${person.name}`);
    } else {
      dispatch(addPerson(person));

      notify(`Added ${person.name}`);
    }

    setEditedPerson(null);

    // Only close dialog if not adding more people
    setIsManagePersonDialogShown(!wasEditing && !shouldClose);
  };

  return (
    <>
      <AppBar goBack={false} logo title="People">
        <AppBar.Action disabled icon="magnify" onPress={() => {}} />
        <AppBar.MenuAction>
          <List.Subheader>Filter People</List.Subheader>
          <List.Item
            right={() => (
              <Switch
                value={isShowingArchived}
                onValueChange={() => setIsShowingArchived(!isShowingArchived)}
              />
            )}
            title="Show archived people"
          />
        </AppBar.MenuAction>
      </AppBar>
      {/* NOTE: Portal FAB must be rendered before other portals to properly place FAB beneath */}
      <PortalFAB
        actions={fabActions}
        icon={isFabOpen ? "close" : "plus"}
        open={isFabOpen}
        style={{ paddingBottom: 54 }}
        visible={isPeopleFabShown}
        onStateChange={({ open }) => setIsFabOpen(open)}
      />
      <ArchivePersonDialog
        person={archivedPerson}
        visible={hasArchival}
        onCancel={onArchivePersonCancel}
        onConfirm={onArchivePersonConfirm}
      />
      <ManagePersonDialog
        addMode={addPersonMode}
        person={editedPerson}
        visible={isManagePersonDialogShown}
        onCancel={onManagePersonCancel}
        onConfirm={onManagePersonConfirm}
      />
      <PeopleList
        people={filteredPeople}
        onArchive={onArchivePersonPress}
        onEdit={onEditPersonPress}
      />
    </>
  );
};

// eslint-disable-next-line no-unused-vars
const styles = StyleSheet.create({});

export default PeopleScreen;
