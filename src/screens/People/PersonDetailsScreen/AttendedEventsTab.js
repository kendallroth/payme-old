import React from "react";
import { StyleSheet, View } from "react-native";
import { useSelector } from "react-redux";

// Components
import AttendedEventsList from "./AttendedEventsList";

// Utilities
import { selectEventsForPerson } from "@store/slices/events";

const AttendedEventsTab = (props) => {
  const { person } = props;

  const attendedEvents = useSelector((state) =>
    selectEventsForPerson(state, person.id),
  );

  return (
    <View style={styles.tab}>
      <AttendedEventsList events={attendedEvents} />
    </View>
  );
};

const styles = StyleSheet.create({
  tab: {
    flex: 1,
    alignItems: "center",
  },
});

export default AttendedEventsTab;
