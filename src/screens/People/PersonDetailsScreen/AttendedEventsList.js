import React, { useState } from "react";
import { FlatList, StyleSheet, View } from "react-native";
import { List } from "react-native-paper";

// Components
import FilterBar from "@components/FilterBar";
import { EmptyView } from "@components/layout";
import UnpaidIndicator from "@components/UnpaidIndicator";

// Utilities
import theme from "@theme";
import { formatDateString } from "@utilities/dates";

const AttendedEventsList = (props) => {
  const { events } = props;

  const [listFilter, setListFilter] = useState("all");

  const filteredEvents =
    listFilter === "all" ? events : events.filter((e) => !e.paidAt);

  const renderEmptyComponent = () => {
    return events.length ? (
      <EmptyView
        image={require("@assets/images/undraw_celebration_blue.png")}
        text="Looks like all their attendance has been paid!"
      />
    ) : (
      <EmptyView
        image={require("@assets/images/undraw_not_found_blue.png")}
        text="Looks like this person has no attendance yet!"
      />
    );
  };

  const renderHeaderComponent = () => (
    <FilterBar
      filters={[
        { color: theme.colors.greyLight, text: "All", value: "all" },
        { color: theme.colors.error, text: "Unpaid", value: "unpaid" },
      ]}
      style={{ marginTop: 16, marginBottom: 8 }}
      value={listFilter}
      onSelect={(filter) => setListFilter(filter)}
    />
  );

  const renderEventItem = ({ item }) => (
    <List.Item
      key={item.id}
      description={formatDateString(item.date)}
      descriptionStyle={styles.listItemDescription}
      right={() => renderEventItemRight(item)}
      title={item.name}
      titleStyle={!item.paidAt && styles.listItemTitleUnpaid}
    />
  );

  const renderEventItemRight = (item) => (
    <View style={theme.styles.listItemActions}>
      <UnpaidIndicator unpaid={!item.paidAt} />
    </View>
  );

  return (
    <FlatList
      data={filteredEvents}
      keyExtractor={(item) => item.id}
      ListEmptyComponent={renderEmptyComponent}
      ListHeaderComponent={renderHeaderComponent}
      renderItem={renderEventItem}
      style={theme.styles.list}
    />
  );
};

const styles = StyleSheet.create({
  listItemDescription: {
    fontStyle: "italic",
  },
  listItemTitleUnpaid: {
    color: theme.colors.error,
    fontWeight: "bold",
  },
});

export default AttendedEventsList;
