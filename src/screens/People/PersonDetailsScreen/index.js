import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dimensions, StyleSheet } from "react-native";
import { TabBar, TabView } from "react-native-tab-view";
import { useNavigation, useRoute } from "@react-navigation/native";

// Components
import { ArchivePersonDialog, ManagePersonDialog } from "@components/dialogs";
import { AppBar, EmptyView } from "@components/layout";
import AttendedEventsTab from "./AttendedEventsTab";

// Utilities
import { useSnackbar } from "@hooks";
import {
  archivePerson,
  selectPerson,
  updatePerson,
} from "@store/slices/people";
import theme from "@theme";

const initialLayout = { width: Dimensions.get("window").width };

const PersonDetailsScreen = () => {
  const { notify } = useSnackbar();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const route = useRoute();

  const [isEditDialogShown, setIsEditDialogShown] = useState(false);
  const [isArchiveDialogShown, setIsArchiveDialogShown] = useState(false);

  const person = useSelector((state) => selectPerson(state, route.params.id));
  const isArchiving = !person.archivedAt;

  // Tabs
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    { key: "events", title: "Events" },
    { key: "groups", title: "Groups" },
  ]);

  // NOTE: Rare edge case if person is not found or deleted...
  if (!person) {
    if (navigation.isFocused()) {
      navigation.popToTop();
    }
    return null;
  }

  const renderScene = ({ route: r }) => {
    switch (r.key) {
      case "events":
        return <AttendedEventsTab person={person} />;
      case "groups":
        return (
          <EmptyView
            image={require("@assets/images/undraw_under_construction_blue.png")}
            style={styles.emptyContainer}
            text="Under Construction"
          />
        );
      default:
        return null;
    }
  };
  const renderTabBar = (props) => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: theme.colors.accent }}
    />
  );

  /**
   * Cancel archiving a person
   */
  const onArchivePersonCancel = () => {
    setIsArchiveDialogShown(false);
  };

  /**
   * Confirm archiving a person
   */
  const onArchivePersonConfirm = () => {
    dispatch(archivePerson(person));

    setIsArchiveDialogShown(false);

    isArchiving && navigation.goBack();
    notify(`${isArchiving ? "Archived" : "Unarchived"} ${person.name}`);
  };

  /**
   * Open the archive person dialog
   */
  const onArchivePersonPress = () => {
    setIsArchiveDialogShown(true);
  };

  /**
   * Open the edit person dialog
   */
  const onEditPersonPress = () => {
    setIsEditDialogShown(true);
  };

  /**
   * Cancel editing a person
   */
  const onEditPersonCancel = () => {
    setIsEditDialogShown(false);
  };

  /**
   * Confirm editing a person
   * @param {Object} updatedPerson - Updated person
   */
  const onEditPersonConfirm = (updatedPerson) => {
    dispatch(updatePerson(updatedPerson));

    setIsEditDialogShown(false);

    notify(`Updated ${updatedPerson.name}`);
  };

  return (
    <>
      <AppBar subtitle={person.archivedAt && "Archived"} title={person.name}>
        <AppBar.Action
          disabled={person.archivedAt}
          icon="pencil"
          onPress={onEditPersonPress}
        />
        <AppBar.Action
          icon={person.archivedAt ? "package-up" : "package-down"}
          onPress={onArchivePersonPress}
        />
      </AppBar>
      <ArchivePersonDialog
        person={person}
        visible={isArchiveDialogShown}
        onCancel={onArchivePersonCancel}
        onConfirm={onArchivePersonConfirm}
      />
      <ManagePersonDialog
        person={person}
        visible={isEditDialogShown}
        onCancel={onEditPersonCancel}
        onConfirm={onEditPersonConfirm}
      />
      <TabView
        initialLayout={initialLayout}
        navigationState={{ index, routes }}
        renderScene={renderScene}
        renderTabBar={renderTabBar}
        onIndexChange={setIndex}
      />
    </>
  );
};

const styles = StyleSheet.create({
  emptyContainer: {
    marginTop: 24,
  },
});

export default PersonDetailsScreen;
