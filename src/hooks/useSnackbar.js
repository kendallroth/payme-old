import { useCallback, useContext } from "react";
import { useFocusEffect } from "@react-navigation/native";

// Utilities
import { SnackbarContext } from "@contexts";

/**
 * Expose snackbar manager context as hook
 * @param   {Object}  options.cleanup     - Snackbar hook options
 * @param   {boolean} dismissOnTransition - Whether snackbar should be dismissed on screen transition
 * @returns {Object}  Snackbar manager
 */
const useSnackbar = (options = {}) => {
  const snackbar = useContext(SnackbarContext);
  const { cleanup: shouldRemoveOnTransition = true } = options;

  // Remove snackbar when leaving page by default
  useFocusEffect(
    useCallback(() => {
      return () => {
        if (shouldRemoveOnTransition) {
          snackbar.closeNotification();
        }
      };
      // NOTE: Referencing 'closeNotification' here will cause infinite loop!
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  return snackbar;
};

export default useSnackbar;
