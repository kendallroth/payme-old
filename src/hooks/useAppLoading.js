import { useContext } from "react";

// Utilities
import { AppLoadingContext } from "@contexts";

/**
 * Expose app loading manager context as hook
 * @returns {Object} App loader manager
 */
const useAppLoading = () => useContext(AppLoadingContext);

export default useAppLoading;
