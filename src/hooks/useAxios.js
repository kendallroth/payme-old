import { useState } from "react";

/**
 * Simple hook to call an Axios API and store the response
 * @param {function} apiFunc     - Specific API function call
 */
const useAxios = (apiFunc) => {
  if (!apiFunc) {
    throw new Error("'useAxios' hook requires an API call function");
  }

  const initialResponse = {
    data: null,
    error: null,
    fetching: false,
    // Whether API has been requested at least once
    requested: false,
    // NOTE: Should not reset on fetch, only on error
    success: false,
  };
  const [response, setResponse] = useState(initialResponse);

  /**
   * Handler to get the API response
   * @param {Any[]}    apiFuncArgs - Arguments for the API function
   */
  const getResponse = (...apiFuncArgs) => {
    setResponse({
      ...response,
      fetching: true,
    });

    return apiFunc(...apiFuncArgs)
      .then(({ data }) => {
        const dataValue = data !== undefined ? data : null;

        setResponse({
          ...response,
          data: dataValue,
          error: null,
          fetching: false,
          success: true,
          requested: true,
        });

        // Data can be accessed by chaining "then" function when getting data
        return dataValue;
      })
      .catch((e) => {
        // Some errors may not have an Axios response (should default to message)
        let error = e.message;
        if (e.response) {
          error = e.response.data || e.message;
        }

        setResponse({
          ...response,
          error,
          fetching: false,
          success: false,
          requested: true,
        });

        // Error can be accessed by chaining "catch" function when getting data.
        //   Note that a "catch" function is always required to catch uncaught promises.
        // Must throw a custom object rather than "Error" in case it contains additional data.
        //   Note that this means it will not show stack traces in Sentry if unhandled...
        throw error;
      });
  };

  /**
   * Reset the API response (ie. clear data before re-fetching)
   */
  const reset = () => setResponse(initialResponse);

  // Additional response helpers
  const responseHelpers = { reset };

  return [response, getResponse, responseHelpers];
};

export default useAxios;
