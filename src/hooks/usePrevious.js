import { useEffect, useRef } from "react";

/**
 * Hook to store the value of a React component prop/state item from last render
 * @param {Any} value - Value to track
 */
function usePrevious(value) {
  // Ref object can hold any value (not just DOM references)
  const ref = useRef();

  // Update the "previous" value of the entity whenever it changes.
  //   NOTE: The actual "previous" will be returned before this update occurs.
  useEffect(() => {
    ref.current = value;
  }, [value]);

  // Return previous value (happens before update in useEffect above)
  return ref.current;
}

export default usePrevious;
