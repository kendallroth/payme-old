import React from "react";
import { Provider } from "react-redux";
import { Provider as PaperProvider } from "react-native-paper";
import { enableScreens } from "react-native-screens";
import { registerRootComponent } from "expo";
import { StatusBar } from "expo-status-bar";

import "react-native-gesture-handler";

// NOTE: Optimize React Navigation memory usage/performance?
// Taken from: https://reactnavigation.org/docs/react-native-screens/
enableScreens();

// Components
import { TheAppDataLoader } from "@components/single";
import Router from "./router";

// Utilities
import ContextProvider from "@contexts/ContextProvider";
import setupStore from "@store";
import theme from "@theme";

const { persistor, store } = setupStore();

const App = () => {
  // NOTE: Redux store provider must be outside PaperProvider!

  return (
    <Provider store={store}>
      <TheAppDataLoader persistor={persistor}>
        <PaperProvider theme={theme}>
          <ContextProvider>
            {/* Necessary to allow transparent status bar? */}
            <StatusBar style="light" />
            <Router />
          </ContextProvider>
        </PaperProvider>
      </TheAppDataLoader>
    </Provider>
  );
};

export default registerRootComponent(App);
