import React from "react";
import PropTypes from "prop-types";
import { StyleSheet } from "react-native";
import { FAB, Portal } from "react-native-paper";
import { useIsFocused } from "@react-navigation/native";

/**
 * FAB with Portal
 *
 * NOTE: This FAB uses a Portal to allow the FAB to animate when leaving a screen
 *         (simply disappears otherwise)!
 *       Additionally, this FAB must be rendered before other portals/dialogs
 *         in order to properly render beneath them.
 *
 */
const PortalFAB = (props) => {
  const { actions, style, visible, onStateChange, ...rest } = props;

  // NOTE: Because FAB uses a portal it must only display when screen is focused
  const isFabShown = useIsFocused();

  /* NOTE: Portal must be rendered before other portals to properly place FAB beneath */
  return (
    <Portal>
      <FAB.Group
        {...rest}
        actions={actions}
        style={[styles.fab, style]}
        visible={visible && isFabShown}
        onStateChange={onStateChange ? onStateChange : () => {}}
      />
    </Portal>
  );
};

PortalFAB.propTypes = {
  actions: PropTypes.array,
  style: PropTypes.object,
  visible: PropTypes.bool,
  onStateChange: PropTypes.func,
  onPress: PropTypes.func,
};

PortalFAB.defaultProps = {
  actions: [],
  style: {},
  visible: true,
  onStateChange: null,
  onPress: null,
};

const styles = StyleSheet.create({
  fab: {
    // Include spacing for navigation bar
    paddingBottom: 54,
  },
});

export default PortalFAB;
