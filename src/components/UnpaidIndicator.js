import React from "react";
import PropTypes from "prop-types";
import { StyleSheet, View } from "react-native";
import { List, Text } from "react-native-paper";

// Utilities
import theme from "@theme";

/**
 * Indicate that the unpaid quantity
 * @param {string}         icon      - Unpaid icon
 * @param {Object}         iconProps - Icon props
 * @param {Object}         iconStyle - Icon style
 * @param {Object}         style     - Indicator style
 * @param {boolean|number} unpaid    - Number unpaid
 */
const UnpaidIndicator = ({ icon, iconProps, iconStyle, style, unpaid }) => {
  if (!unpaid) return null;

  // Only display unpaid amount (text) if a number is specified
  const isBoolean = unpaid === true;

  return (
    <View style={[styles.unpaid, style]}>
      {!isBoolean && <Text style={styles.unpaidText}>{unpaid}</Text>}
      <List.Icon
        {...iconProps}
        color={theme.colors.error}
        icon={icon}
        style={[styles.unpaidIcon, iconStyle]}
      />
    </View>
  );
};

UnpaidIndicator.propTypes = {
  icon: PropTypes.string,
  iconProps: PropTypes.object,
  iconStyle: PropTypes.object,
  style: PropTypes.object,
  unpaid: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
};

UnpaidIndicator.defaultProps = {
  icon: "alert-circle",
  iconProps: {},
  iconStyle: {},
  style: {},
  unpaid: false,
};

const styles = StyleSheet.create({
  unpaid: {
    flexDirection: "row",
    alignItems: "center",
  },
  unpaidIcon: {
    margin: 0,
    padding: 0,
    height: 32,
    width: 32,
  },
  unpaidText: {
    fontSize: 18,
    fontWeight: "bold",
  },
});

export default UnpaidIndicator;
