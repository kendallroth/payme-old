import React from "react";
import PropTypes from "prop-types";
import { Image, StatusBar, StyleSheet } from "react-native";
import { Appbar as BaseAppBar } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";

// Components
import AppBarMenu from "./AppBarMenu";

const AppBar = (props) => {
  const { children, goBack, logo, subtitle, title } = props;

  const navigation = useNavigation();

  return (
    <BaseAppBar.Header
      statusBarHeight={StatusBar.currentHeight}
      style={styles.header}
    >
      {goBack && <BaseAppBar.BackAction onPress={navigation.goBack} />}
      {logo && (
        <Image
          fadeDuration={100}
          resizeMode="contain"
          source={require("@assets/icons/logo.png")}
          style={styles.headerLogo}
        />
      )}
      <BaseAppBar.Content subtitle={subtitle} title={title} />
      {children}
    </BaseAppBar.Header>
  );
};

const styles = StyleSheet.create({
  header: {
    elevation: 0,
    shadowOpacity: 0,
  },
  headerLogo: {
    width: 32,
    height: 32,
    marginLeft: 8,
  },
});

AppBar.propTypes = {
  goBack: PropTypes.bool,
  logo: PropTypes.bool,
  subtitle: PropTypes.string,
  title: PropTypes.string.isRequired,
};

AppBar.defaultProps = {
  goBack: true,
  logo: false,
  subtitle: null,
};

AppBar.Action = BaseAppBar.Action;
AppBar.MenuAction = AppBarMenu;

export default AppBar;
