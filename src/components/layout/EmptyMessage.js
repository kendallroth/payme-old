import React from "react";
import PropTypes from "prop-types";
import { StyleSheet } from "react-native";
import { Text } from "react-native-paper";

// Utilities
import theme from "@theme";

/**
 * Empty message (indicator)
 * @param {string}  children - React children
 * @param {Object}  style    - Style object
 * @param {string}  text     - Empty indicator text (if not children)
 * @param {boolean} visible  - Whether component is visible (defaults to true)
 */
const EmptyMessage = ({ children, style, text, visible }) => {
  if (!visible) return null;

  return (
    <Text style={[styles.emptyText, style]}>
      {children || text || "No items"}
    </Text>
  );
};

EmptyMessage.propTypes = {
  children: PropTypes.string,
  style: PropTypes.object,
  text: PropTypes.string,
  visible: PropTypes.bool,
};

EmptyMessage.defaultProps = {
  children: null,
  style: {},
  text: null,
  visible: true,
};

const styles = StyleSheet.create({
  emptyText: {
    padding: 16,
    color: theme.colors.greyDark,
  },
});

export default EmptyMessage;
