import React, { useState } from "react";
import PropTypes from "prop-types";
import { Appbar } from "react-native-paper";
import { StyleSheet } from "react-native";
import { Menu } from "react-native-paper";

const AppBarMenu = (props) => {
  const { children, icon, style } = props;

  const [isMenuOpen, setIsMenuOpen] = useState(false);

  return (
    <Menu
      anchor={
        <Appbar.Action
          color="white"
          icon={icon}
          onPress={() => setIsMenuOpen(true)}
        />
      }
      style={[styles.menuStyle, style]}
      visible={isMenuOpen}
      onDismiss={() => setIsMenuOpen(false)}
    >
      {children}
    </Menu>
  );
};

AppBarMenu.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.node]),
  icon: PropTypes.string,
  style: PropTypes.object,
};

AppBarMenu.defaultProps = {
  icon: "dots-vertical",
  style: {},
};

const styles = StyleSheet.create({
  menuStyle: {
    top: 24,
    minWidth: 250,
  },
});

export default AppBarMenu;
