import React from "react";
import PropTypes from "prop-types";
import { StyleSheet, View } from "react-native";
import { Text } from "react-native-paper";

// Components
import ScalableImage from "@components/ScalableImage";

// Utilities
import theme from "@theme";

/**
 * Empty view (indicator)
 * @param {string}  children   - React children
 * @param {string}  image      - Image source
 * @param {number}  imageWidth - Image width
 * @param {Object}  style      - Style object
 * @param {string}  text       - Empty indicator text
 * @param {Object}  textStyle  - Text style object
 * @param {boolean} visible    - Whether component is visible (defaults to true)
 */
const EmptyView = ({
  image,
  imageWidth = 300,
  style,
  text,
  textStyle,
  visible,
}) => {
  if (!visible || !image) return null;

  return (
    <View style={[styles.emptyView, style]}>
      <ScalableImage resizeMode="cover" source={image} width={imageWidth} />
      {text && <Text style={[styles.emptyViewText, textStyle]}>{text}</Text>}
    </View>
  );
};

EmptyView.propTypes = {
  children: PropTypes.string,
  image: PropTypes.number.isRequired,
  imageWidth: PropTypes.number,
  style: PropTypes.object,
  text: PropTypes.string,
  textStyle: PropTypes.object,
  visible: PropTypes.bool,
};

EmptyView.defaultProps = {
  children: null,
  imageWidth: 300,
  style: {},
  text: null,
  textStyle: {},
  visible: true,
};

const styles = StyleSheet.create({
  emptyView: {
    alignItems: "center",
    margin: 24,
  },
  emptyViewText: {
    marginTop: 16,
    fontWeight: "bold",
    fontSize: 16,
    color: theme.colors.greyDark,
  },
});

export default EmptyView;
