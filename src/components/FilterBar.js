import React from "react";
import PropTypes from "prop-types";
import { StyleSheet, View } from "react-native";
import { Surface, Text, TouchableRipple } from "react-native-paper";

// Utilities
import theme from "@theme";

const FilterBar = (props) => {
  const { filters, style, value, onSelect } = props;
  if (!filters || !filters.length) return null;

  return (
    <View style={styles.filterContainer}>
      <Surface elevation={4} style={[styles.filterBar, style]}>
        {filters.map((f) => {
          const isSelected = f.value === value;

          return (
            <TouchableRipple
              key={f.value}
              style={[
                styles.filterBarFilter,
                isSelected ? styles.filterBarFilterSelected : null,
              ]}
              onPress={() => onSelect(f.value)}
            >
              <>
                <View
                  style={[
                    styles.filterBarFilterIcon,
                    { backgroundColor: f.color },
                  ]}
                />
                <Text style={styles.filterBarFilterText}>{f.text}</Text>
              </>
            </TouchableRipple>
          );
        })}
      </Surface>
    </View>
  );
};

FilterBar.propTypes = {
  filters: PropTypes.arrayOf(PropTypes.object).isRequired,
  style: PropTypes.object,
  value: PropTypes.string.isRequired,
  onSelect: PropTypes.func.isRequired,
};

FilterBar.defaultProps = {
  style: {},
};

const styles = StyleSheet.create({
  filterContainer: {
    alignItems: "center",
  },
  filterBar: {
    flexDirection: "row",
    borderRadius: 4,
    overflow: "hidden",
  },
  filterBarFilter: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    maxWidth: 120,
    padding: 8,
    borderWidth: 2,
    borderColor: "transparent",
  },
  filterBarFilterSelected: {
    borderColor: theme.colors.primary,
  },
  filterBarFilterIcon: {
    width: 20,
    height: 20,
    marginRight: 8,
    borderRadius: 4,
  },
  filterBarFilterText: {
    textTransform: "uppercase",
  },
});

export default FilterBar;
