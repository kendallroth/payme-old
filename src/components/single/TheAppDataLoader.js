/*
 * Component to keep splash screen visible until vital data is loaded.
 *
 * Adapted from: https://github.com/rt2zz/redux-persist/blob/master/src/integration/react.js
 */

import { useEffect } from "react";
import PropTypes from "prop-types";
import * as SplashScreen from "expo-splash-screen";

/**
 * Data Loader to keep splash screen until important data is loaded
 * @param {Object} persistor - Redux store persistor reference
 */
const TheAppDataLoader = (props) => {
  const { children, persistor } = props;

  useEffect(() => {
    const unsubscribe = persistor.subscribe(handlePersistor);

    // Prevent hiding the splash screen automatically
    const preventSplashScreenHide = async () => {
      try {
        await SplashScreen.preventAutoHideAsync();
      } catch {
        // NOTE: Drop error
      }
    };

    preventSplashScreenHide();

    handlePersistor(unsubscribe);

    return unsubscribe;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  /**
   * Hide the splash screen once all data has loaded
   *
   * NOTE: This is currently just Redux Persist!
   *
   * @param {function} unsubscribe - Persistor watch unsubscribe function
   */
  const handlePersistor = (unsubscribe = null) => {
    const { bootstrapped } = persistor.getState();
    if (!bootstrapped) return;

    // NOTE: Use tiny timeout to avoid flickering after loading all data
    setTimeout(() => {
      SplashScreen.hideAsync();

      unsubscribe && unsubscribe();
    }, 100);
  };

  return children;
};

TheAppDataLoader.propTypes = {
  persistor: PropTypes.object.isRequired,
};

export default TheAppDataLoader;
