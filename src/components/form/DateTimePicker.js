import React from "react";
import PropTypes from "prop-types";
import BaseDateTimePicker from "@react-native-community/datetimepicker";
import { formatISO, parseISO } from "date-fns";

/**
 * DateTimePicker wrapper to handle ISO date conversion (component uses Date objects)
 * @param {Object}   innerRef - Native TextInput ref
 * @param {string}   value    - ISO date string
 * @param {function} onChange - Change handler
 */
const DateTimePicker = ({ innerRef, value, onChange, ...props }) => {
  const pickerDate = parseISO(value);

  /**
   * Wrap the picker to return an ISO string rather than a date (for Redux, db, etc)
   * @param {Event} e     - Event object
   * @param {Date}  value - Selected date value
   */
  // NOTE: Close event has an undefined value!
  const onDateChange = (e, date) => {
    if (!date) return onChange();

    // NOTE: Must convert Date object to ISO string!
    onChange(formatISO(date));
  };

  return (
    <BaseDateTimePicker
      ref={innerRef}
      {...props}
      value={pickerDate}
      onChange={onDateChange}
    />
  );
};

DateTimePicker.propTypes = {
  innerRef: PropTypes.object,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

DateTimePicker.defaultProps = {
  innerRef: null,
};

export default DateTimePicker;
