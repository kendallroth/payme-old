import React, {
  forwardRef,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import PropTypes from "prop-types";
import { StyleSheet } from "react-native";
import { TextInput } from "react-native-paper";
import { useField } from "formik";

// Components
import DateTimePicker from "@components/form/DateTimePicker";
import FormikTextInput from "@components/form/TextInput";

// Utilities
import { formatDateString } from "@utilities/dates";

/**
 * Date/time picker input
 * @param {string} name        - Formik field name
 * @param {Object} pickerProps - Datepicker props
 */
const DateTimeInput = forwardRef(({ name, pickerProps, ...props }, ref) => {
  const dateInputRef = useRef(null);
  const [isDateShown, setIsDateShown] = useState(false);
  const [, meta, handlers] = useField(name);

  const { value } = meta;
  const { setValue } = handlers;

  // NOTE: Expose the date picker toggle via a child ref (allow parent to toggle dialog)!
  // Taken from: https://stackoverflow.com/a/37950970/4206438
  useImperativeHandle(ref, () => ({
    toggleDatePicker,
  }));

  // NOTE: Must also handle close event (with undefined value)!
  const onDateChange = (date) => {
    toggleDatePicker(false);
    if (!date) return;

    setValue(date);
  };

  /**
   * Change whether the date picker is shown
   * @param {boolean} isShown - Whether the date picker should be shown
   */
  const toggleDatePicker = (isShown) => {
    if (isShown) {
      dateInputRef.current.focus();
    }

    setIsDateShown(isShown);
  };

  return (
    <>
      <FormikTextInput
        innerRef={dateInputRef}
        {...props}
        displayValue={formatDateString(value)}
        name={name}
        right={
          <TextInput.Icon
            icon="calendar"
            onPress={() => toggleDatePicker(true)}
          />
        }
      />
      {isDateShown && (
        <DateTimePicker
          {...pickerProps}
          mode="date"
          value={value}
          onChange={onDateChange}
        />
      )}
    </>
  );
});

DateTimeInput.propTypes = {
  name: PropTypes.string.isRequired,
  pickerProps: PropTypes.object,
};

DateTimeInput.defaultProps = {
  pickerProps: {},
};

// eslint-disable-next-line no-unused-vars
const styles = StyleSheet.create({});

export default DateTimeInput;
