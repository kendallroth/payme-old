import React from "react";
import PropTypes from "prop-types";
import { StyleSheet } from "react-native";
import { HelperText, TextInput as BaseTextInput } from "react-native-paper";
import { useField } from "formik";

/**
 * Wrapped Formik text input
 * @param {string} displayValue - Read-only display value (disables input!)
 * @param {Object} innerRef     - Native TextInput ref
 * @param {string} name         - Formik field name
 */
const TextInput = ({ displayValue, innerRef, name, ...props }) => {
  const [field, meta] = useField(name);

  const { onBlur, onChange } = field;
  const { error, touched, value } = meta;

  const isErrorShown = error && touched;

  // Passing a static display value should "disable" input (but allow focusing field!)
  const fieldValue = displayValue || value;
  const fieldChange = displayValue ? () => {} : onChange(name);

  return (
    <>
      <BaseTextInput
        ref={innerRef}
        mode="outlined"
        {...props}
        error={isErrorShown}
        value={fieldValue}
        onBlur={onBlur(name)}
        onChangeText={fieldChange}
      />
      <HelperText type="error" visible={isErrorShown}>
        {/* NOTE: Need an extra check ('visible' flickers briefly...) */}
        {isErrorShown ? error : ""}
      </HelperText>
    </>
  );
};

TextInput.propTypes = {
  displayValue: PropTypes.string,
  innerRef: PropTypes.object,
  name: PropTypes.string.isRequired,
};

TextInput.defaultProps = {
  displayValue: null,
  innerRef: null,
};

// eslint-disable-next-line no-unused-vars
const styles = StyleSheet.create({});

export default TextInput;
