export { default as ArchivePersonDialog } from "./ArchivePersonDialog";
export { default as ArchiveEventDialog } from "./ArchiveEventDialog";
export { default as ConfirmDialog } from "./ConfirmDialog";
export { default as ManageEventDialog } from "./ManageEventDialog";
export { default as ManagePersonDialog } from "./ManagePersonDialog";
