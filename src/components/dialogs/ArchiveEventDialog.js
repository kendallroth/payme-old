import React from "react";
import PropTypes from "prop-types";
import { StyleSheet } from "react-native";
import { Text } from "react-native-paper";

// Components
import ConfirmDialog from "./ConfirmDialog";

/**
 * Archive event dialog
 * @param {Object}   event       - Event for archival
 * @param {boolean}  visible     - Whether the dialog is visible
 * @param {function} onCancel    - Cancel handler
 * @param {function} onConfirm   - Confirmation handler
 */
const ArchiveEventDialog = (props) => {
  const { event, visible, onCancel, onConfirm } = props;
  if (!event) return null;

  const isArchiving = !event.archivedAt;

  return (
    <ConfirmDialog
      title={isArchiving ? "Archive Event" : "Unarchive Event"}
      visible={visible}
      onCancel={onCancel}
      onConfirm={onConfirm}
    >
      <Text>
        Are you sure you want to {isArchiving ? "archive" : "unarchive"} this
        event?
      </Text>
      <Text style={styles.archiveDialogEvent}>{event.name}</Text>
    </ConfirmDialog>
  );
};

ArchiveEventDialog.propTypes = {
  event: PropTypes.object,
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
};

ArchiveEventDialog.defaultProps = {
  event: null,
};

const styles = StyleSheet.create({
  archiveDialogEvent: {
    marginTop: 16,
    fontWeight: "bold",
  },
});

export default ArchiveEventDialog;
