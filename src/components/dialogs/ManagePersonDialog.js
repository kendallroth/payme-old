import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { StyleSheet } from "react-native";
import { Button, Dialog, Portal } from "react-native-paper";
import { useFormik, FormikContext } from "formik";
import * as Yup from "yup";

// Components
import TextInput from "@components/form/TextInput";

// Utilities
import theme from "@theme";

const initialPerson = {
  name: "",
};

const PersonSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "Name is too short")
    .max(25, "Name is too long")
    .required("Name is required"),
});

/**
 * Add/edit person dialog
 * @param {boolean}  addMode   - Whether multiple people are being added
 * @param {Object}   person    - Initial person
 * @param {boolean}  visible   - Whether the dialog is visible
 * @param {function} onCancel  - Cancel handler
 * @param {function} onConfirm - Confirmation handler
 */
const AddEditPersonDialog = ({
  addMode,
  person: editedPerson,
  visible,
  onCancel,
  onConfirm,
}) => {
  const isEditing = Boolean(editedPerson);
  const isAddingMultiple = !isEditing && addMode === "multiple";

  const addButtonText = isEditing ? "Save" : isAddingMultiple ? "Next" : "Add";
  const titleText = isEditing
    ? "Update Person"
    : isAddingMultiple
    ? "Add People"
    : "Add Person";

  const formik = useFormik({
    // NOTE: "Hack" because 'resetForm' is not working as expected!
    initialValues: editedPerson || initialPerson,
    validationSchema: PersonSchema,
    onSubmit: (fields) => {
      // Reset dialog (and keep open) if adding another person
      if (isAddingMultiple) {
        onConfirm(fields, false);
        resetForm(initialPerson);
      } else {
        onConfirm(fields);
      }
    },
  });

  const { resetForm, submitForm } = formik;

  // Clear form whenever visibility changes
  useEffect(() => {
    resetForm(initialPerson);
  }, [resetForm, visible]);

  // Set the person's details if provided
  useEffect(() => {
    if (editedPerson) {
      resetForm(editedPerson);
    } else {
      resetForm(initialPerson);
    }
  }, [resetForm, editedPerson]);

  return (
    <Portal>
      <FormikContext.Provider value={formik}>
        <Dialog dismissable={false} visible={visible} onDismiss={onCancel}>
          <Dialog.Title>{titleText}</Dialog.Title>
          <Dialog.Content>
            <TextInput autoFocus label="Name" name="name" />
          </Dialog.Content>
          <Dialog.Actions>
            <Button color={theme.colors.black} onPress={onCancel}>
              Cancel
            </Button>
            <Button onPress={submitForm}>{addButtonText}</Button>
          </Dialog.Actions>
        </Dialog>
      </FormikContext.Provider>
    </Portal>
  );
};

AddEditPersonDialog.propTypes = {
  mode: PropTypes.oneOf(["multiple", "single"]),
  person: PropTypes.object,
  visible: PropTypes.bool,
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
};

AddEditPersonDialog.defaultProps = {
  mode: "single",
  person: null,
  visible: false,
};

// eslint-disable-next-line no-unused-vars
const styles = StyleSheet.create({});

export default AddEditPersonDialog;
