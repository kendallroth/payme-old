import React from "react";
import PropTypes from "prop-types";
import { StyleSheet } from "react-native";
import { Text } from "react-native-paper";

// Components
import ConfirmDialog from "./ConfirmDialog";

/**
 * Archive person dialog
 * @param {Object}   person      - Person for archival
 * @param {boolean}  visible     - Whether the dialog is visible
 * @param {function} onCancel    - Cancel handler
 * @param {function} onConfirm   - Confirmation handler
 */
const ArchivePersonDialog = (props) => {
  const { person, visible, onCancel, onConfirm } = props;
  if (!person) return null;

  const isArchiving = !person.archivedAt;

  return (
    <ConfirmDialog
      title={isArchiving ? "Archive Person" : "Unarchive Person"}
      visible={visible}
      onCancel={onCancel}
      onConfirm={onConfirm}
    >
      <Text>
        Are you sure you want to {isArchiving ? "archive" : "unarchive"} this
        person?
      </Text>
      <Text style={styles.archiveDialogPerson}>{person.name}</Text>
    </ConfirmDialog>
  );
};

ArchivePersonDialog.propTypes = {
  person: PropTypes.object,
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
};

ArchivePersonDialog.defaultProps = {
  person: null,
};

const styles = StyleSheet.create({
  archiveDialogPerson: {
    marginTop: 16,
    fontWeight: "bold",
  },
});

export default ArchivePersonDialog;
