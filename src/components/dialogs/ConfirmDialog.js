import React from "react";
import PropTypes from "prop-types";
import { StyleSheet } from "react-native";
import { Button, Dialog, Portal } from "react-native-paper";

// Utilities
import theme from "@theme";

/**
 * Confirmation dialog
 * @param {string}   cancelText  - Cancel button text
 * @param {string}   confirmText - Confirm button text
 * @param {string}   title       - Dialog title
 * @param {boolean}  visible     - Whether the dialog is visible
 * @param {function} onCancel    - Cancel handler
 * @param {function} onConfirm   - Confirmation handler
 */
const ConfirmDialog = ({
  cancelText,
  children,
  confirmText,
  title,
  visible,
  onCancel,
  onConfirm,
}) => (
  <Portal>
    <Dialog dismissable={false} visible={visible} onDismiss={onCancel}>
      <Dialog.Title>{title}</Dialog.Title>
      <Dialog.Content>{children}</Dialog.Content>
      <Dialog.Actions>
        <Button color={theme.colors.black} onPress={onCancel}>
          {cancelText}
        </Button>
        <Button onPress={onConfirm}>{confirmText}</Button>
      </Dialog.Actions>
    </Dialog>
  </Portal>
);

ConfirmDialog.propTypes = {
  cancelText: PropTypes.string,
  confirmText: PropTypes.string,
  title: PropTypes.string.isRequired,
  visible: PropTypes.bool,
  onCancel: PropTypes.func,
  onConfirm: PropTypes.func.isRequired,
};

ConfirmDialog.defaultProps = {
  cancelText: "Cancel",
  confirmText: "Confirm",
  visible: false,
};

// eslint-disable-next-line no-unused-vars
const styles = StyleSheet.create({});

export default ConfirmDialog;
