import React, { useEffect, useRef } from "react";
import PropTypes from "prop-types";
import { StyleSheet } from "react-native";
import { Button, Dialog, Portal } from "react-native-paper";
import { formatISO } from "date-fns";
import { useFormik, FormikContext } from "formik";
import * as Yup from "yup";

// Components
import DateTimeInput from "@components/form/DateTimeInput";
import TextInput from "@components/form/TextInput";

const initialEvent = {
  date: formatISO(new Date()),
  name: "",
};

const EventSchema = Yup.object().shape({
  date: Yup.string().required("Date is required"),
  name: Yup.string()
    .min(2, "Name is too short")
    .max(25, "Name is too long")
    .required("Name is required"),
});

/**
 * Add/edit event dialog
 * @param {Object}   event     - Initial event
 * @param {boolean}  visible   - Whether the dialog is visible
 * @param {function} onCancel  - Cancel handler
 * @param {function} onConfirm - Confirmation handler
 */
const AddEditEventDialog = ({
  event: editedEvent,
  visible,
  onCancel,
  onConfirm,
}) => {
  const datePickerRef = useRef(null);

  const isEditing = Boolean(editedEvent);

  const formik = useFormik({
    // NOTE: "Hack" because 'resetForm' is not working as expected!
    initialValues: editedEvent || initialEvent,
    validationSchema: EventSchema,
    onSubmit: (fields) => {
      onConfirm(fields);
    },
  });

  const { resetForm, submitForm } = formik;

  // Clear form whenever visibility changes
  useEffect(() => {
    resetForm(initialEvent);
    hideDatePicker();
  }, [resetForm, visible]);

  // Set the event details if provided
  useEffect(() => {
    if (editedEvent) {
      resetForm(editedEvent);
    } else {
      resetForm(initialEvent);
    }
  }, [resetForm, editedEvent]);

  /**
   * Hide the child input date picker (via a child 'forwardRef')
   *
   * QUESTION: Is this actually necessary?
   */
  const hideDatePicker = () => {
    if (!datePickerRef.current) return;

    datePickerRef.current.toggleDatePicker(false);
  };

  return (
    <Portal>
      <FormikContext.Provider value={formik}>
        <Dialog dismissable={false} visible={visible} onDismiss={onCancel}>
          <Dialog.Title>
            {!isEditing ? "Add Event" : "Update Event"}
          </Dialog.Title>
          <Dialog.Content>
            <TextInput autoFocus label="Name" name="name" />
            <DateTimeInput ref={datePickerRef} label="Date" name="date" />
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={onCancel}>Cancel</Button>
            <Button onPress={submitForm}>{!isEditing ? "Add" : "Save"}</Button>
          </Dialog.Actions>
        </Dialog>
      </FormikContext.Provider>
    </Portal>
  );
};

AddEditEventDialog.propTypes = {
  event: PropTypes.object,
  visible: PropTypes.bool,
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
};

AddEditEventDialog.defaultProps = {
  event: null,
  visible: false,
};

// eslint-disable-next-line no-unused-vars
const styles = StyleSheet.create({});

export default AddEditEventDialog;
