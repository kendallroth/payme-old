import { combineReducers } from "@reduxjs/toolkit";

// Utilities
import attendanceReducer from "@store/slices/attendance";
import eventReducer from "@store/slices/events";
import peopleReducer from "@store/slices/people";
import settingsReducer from "@store/slices/settings";

const reducers = combineReducers({
  attendance: attendanceReducer,
  events: eventReducer,
  people: peopleReducer,
  settings: settingsReducer,
});

export default reducers;
