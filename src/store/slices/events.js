import {
  createEntityAdapter,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import { formatISO } from "date-fns";
import { v4 as uuid4v } from "uuid";

// Utilities
import { addDebugDataAction, resetStoreAction } from "@store/actions";
import { DEBUG_events } from "@utilities/debug";
import { selectAttendance } from "./attendance";

const eventsAdapter = createEntityAdapter({
  sortComparer: (a, b) => b.date.localeCompare(a.date),
});

const eventsSelectors = eventsAdapter.getSelectors((state) => state.events);

const eventsSlice = createSlice({
  name: "events",
  initialState: eventsAdapter.getInitialState(),
  reducers: {
    addEvent: (state, action) => {
      const newEvent = {
        ...action.payload,
        id: uuid4v(),
        attendees: [],
      };
      eventsAdapter.addOne(state, newEvent);
    },
    archiveEvent: (state, action) => {
      const archivedEvent = state.entities[action.payload.id];
      if (!archivedEvent) return;

      archivedEvent.archivedAt = archivedEvent.archivedAt
        ? null
        : formatISO(new Date());
    },
    toggleEventAttendee: (state, action) => {
      const { attending, eventId, personId } = action.payload;

      const event = state.entities[eventId];
      if (!event) return;

      const attendeeIdx = event.attendees.findIndex(
        (a) => a.personId === personId,
      );
      const alreadyAttending = attendeeIdx >= 0;

      // Handle updating an event attendee (adding/removing)
      if (attending && !alreadyAttending) {
        event.attendees.push({ personId, paidAt: null });
      } else if (!attending && alreadyAttending) {
        event.attendees.splice(attendeeIdx, 1);
      }
    },
    toggleEventAttendeePaid: (state, action) => {
      const { eventId, paid, personId } = action.payload;

      const event = state.find((e) => e.id === eventId);
      if (!event) return;

      const attendee = event.attendees.find((a) => a.personId === personId);
      if (!attendee) return;

      attendee.paidAt = paid ? formatISO(new Date()) : null;
    },
    updateEvent: (state, action) => {
      const { id, date, name } = action.payload;

      eventsAdapter.updateOne(state, {
        id,
        changes: { date, name },
      });
    },
  },
  extraReducers: {
    [addDebugDataAction]: (state) => {
      eventsAdapter.addMany(state, DEBUG_events);
    },
    [resetStoreAction]: (state) => {
      eventsAdapter.removeAll(state);
    },
  },
});

// Specific event
export const selectEvent = (state, id) => eventsSelectors.selectById(state, id);
// List of events (sorted by ID)
export const selectEvents = eventsSelectors.selectAll;
// Count of events
export const selectEventsCount = eventsSelectors.selectTotal;
// List of events for a person (sorted by ID)
export const selectEventsForPerson = (state, personId) => {
  return state.events.ids.reduce((accum, eId) => {
    const event = state.events.entities[eId];
    const attendee = Object.values(state.attendance.entities).find(
      (a) => a.personId === personId && a.eventId === eId,
    );
    if (!attendee) return accum;

    return [...accum, { ...event, paidAt: attendee.paidAt }];
  }, []);
};
// List of events with attendance (sorted by ID)
export const selectEventsWithAttendance = createSelector(
  [selectEvents, selectAttendance],
  (events, attendance) => {
    return events.map((e) => {
      const stats = attendance.reduce(
        (accum, a) => {
          if (a.eventId !== e.id) return accum;
          return {
            total: accum.total + 1,
            unpaid: a.paidAt ? accum.unpaid : accum.unpaid + 1,
          };
        },
        { unpaid: 0, total: 0 },
      );
      return { ...e, ...stats };
    });
  },
);

// Count of unpaid events
export const selectUnpaidEventCount = createSelector(
  [selectEventsWithAttendance],
  (events) => events.filter((e) => e.unpaid).length,
);

export const {
  addEvent,
  archiveEvent,
  updateEvent,
  toggleEventAttendee,
  toggleEventAttendeePaid,
} = eventsSlice.actions;

export default eventsSlice.reducer;
