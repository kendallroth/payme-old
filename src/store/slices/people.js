import {
  createEntityAdapter,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import { formatISO } from "date-fns";
import { v4 as uuid4v } from "uuid";

// Utilities
import { addDebugDataAction, resetStoreAction } from "@store/actions";
import { DEBUG_people } from "@utilities/debug";
import { selectAttendance } from "./attendance";

const peopleAdapter = createEntityAdapter({
  sortComparer: (a, b) => a.name.localeCompare(b.name),
});

const peopleSelectors = peopleAdapter.getSelectors((state) => state.people);

const peopleSlice = createSlice({
  name: "people",
  initialState: peopleAdapter.getInitialState(),
  reducers: {
    addPerson: (state, action) => {
      const newPerson = {
        ...action.payload,
        id: uuid4v(),
        archivedAt: null,
      };
      peopleAdapter.addOne(state, newPerson);
    },
    archivePerson: (state, action) => {
      const archivedPerson = state.entities[action.payload.id];
      if (!archivedPerson) return;

      archivedPerson.archivedAt = archivedPerson.archivedAt
        ? null
        : formatISO(new Date());
    },
    updatePerson: (state, action) => {
      const { id, name } = action.payload;

      peopleAdapter.updateOne(state, {
        id,
        changes: { name },
      });
    },
  },
  extraReducers: {
    [addDebugDataAction]: (state) => {
      peopleAdapter.addMany(state, DEBUG_people);
    },
    [resetStoreAction]: (state) => {
      peopleAdapter.removeAll(state);
    },
  },
});

// Specific person
export const selectPerson = (state, id) =>
  peopleSelectors.selectById(state, id);
// List of people (sorted by ID)
export const selectPeople = peopleSelectors.selectAll;
// Total count of people
export const selectPeopleCount = peopleSelectors.selectTotal;
// List of people with attendance stats (sorted by ID)
export const selectPeopleWithAttendance = createSelector(
  [selectPeople, selectAttendance],
  (people, attendance) => {
    return people.map((p) => {
      const stats = attendance.reduce(
        (accum, a) => {
          if (a.personId !== p.id) return accum;
          return {
            total: accum.total + 1,
            unpaid: a.paidAt ? accum.unpaid : accum.unpaid + 1,
          };
        },
        { unpaid: 0, total: 0 },
      );
      return { ...p, ...stats };
    });
  },
);
// List of people with attendance of a given event (sorted by IDs)
export const selectPeopleWithAttendanceForEvent = (state, eventId) => {
  return state.people.ids.reduce((accum, personId) => {
    const person = state.people.entities[personId];
    // Hide archived people in the event "people" list
    if (person.archivedAt) return accum;

    const attendance = Object.values(state.attendance.entities).find(
      (a) => a.personId === person.id && a.eventId === eventId,
    );

    const personWithAttendance = {
      ...person,
      attending: Boolean(attendance),
      paidAt: attendance ? Boolean(attendance.paidAt) : false,
    };
    return [...accum, personWithAttendance];
  }, []);
};
// Count of unpaid people
export const selectUnpaidPeopleCount = createSelector(
  [selectPeopleWithAttendance],
  (people) => people.filter((p) => p.unpaid).length,
);

export const { addPerson, archivePerson, updatePerson } = peopleSlice.actions;

export default peopleSlice.reducer;
