import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

// Utilities
import { addDebugDataAction, resetStoreAction } from "@store/actions";

const initialState = {};

const settingsSlice = createSlice({
  name: "settings",
  initialState,
  reducers: {},
  extraReducers: {},
});

// Add debug data to the store
const addDebugData = createAsyncThunk(
  "settings/addDebugData",
  async (arg, { dispatch }) => {
    // NOTE: Delay the action to make it feel that something is happening
    await new Promise((resolve) => setTimeout(resolve, 500));

    await dispatch(addDebugDataAction());

    return true;
  },
);

// Reset the store state
const resetApp = createAsyncThunk(
  "settings/resetApp",
  async (arg, { dispatch }) => {
    // NOTE: Delay the action to make it feel that something is happening
    await new Promise((resolve) => setTimeout(resolve, 500));

    await dispatch(resetStoreAction());

    return true;
  },
);

export { addDebugData, resetApp };

export default settingsSlice.reducer;
