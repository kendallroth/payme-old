import { createEntityAdapter, createSlice } from "@reduxjs/toolkit";
import { formatISO } from "date-fns";
import sort from "fast-sort";
import { v4 as uuid4v } from "uuid";

// Utilities
import { addDebugDataAction, resetStoreAction } from "@store/actions";
import { DEBUG_attendance } from "@utilities/debug";

const attendanceAdapter = createEntityAdapter();

const attendanceSelectors = attendanceAdapter.getSelectors(
  (state) => state.attendance,
);

const initialState = attendanceAdapter.getInitialState();

const attendanceSlice = createSlice({
  name: "attendance",
  initialState,
  reducers: {
    toggleAttendance: (state, action) => {
      const { attending, eventId, personId } = action.payload;

      const attendance = Object.values(state.entities).find(
        (a) => a.eventId === eventId && a.personId === personId,
      );
      const alreadyAttending = Boolean(attendance);

      // Handle updating an event attendee (adding/removing)
      if (attending && !alreadyAttending) {
        const newAttendance = {
          id: uuid4v(),
          eventId,
          personId,
          paidAt: null,
        };
        attendanceAdapter.addOne(state, newAttendance);
      } else if (!attending && alreadyAttending) {
        attendanceAdapter.removeOne(state, attendance.id);
      }
    },
    toggleAttendancePaid: (state, action) => {
      const { eventId, paid, personId } = action.payload;

      const attendance = Object.values(state.entities).find(
        (a) => a.personId === personId && a.eventId === eventId,
      );
      if (!attendance) return;

      attendance.paidAt = paid ? formatISO(new Date()) : null;
    },
  },
  extraReducers: {
    [addDebugDataAction]: (state) => {
      attendanceAdapter.addMany(state, DEBUG_attendance);
    },
    [resetStoreAction]: (state) => {
      attendanceAdapter.removeAll(state);
    },
  },
});

// List of attendance
export const selectAttendance = attendanceSelectors.selectAll;
// List of event attendees (sorted)
export const selectEventAttendees = (state, eventId) => {
  const attendees = Object.values(state.attendance.entities).reduce(
    (accum, attendance) => {
      if (attendance.eventId !== eventId) return accum;

      const person = state.people.entities[attendance.personId];
      if (!person) return accum;

      return [...accum, { ...person, paidAt: attendance.paidAt }];
    },
    [],
  );

  return sort(attendees).asc("name");
};

// Raw list of attendance entities for an event
export const selectRawAttendanceForEvent = (state, eventId) => {
  return Object.values(state.attendance.entities).filter(
    (a) => a.eventId === eventId,
  );
};
// Raw list of attendance entities for a person
export const selectRawAttendanceForPerson = (state, personId) => {
  return Object.values(state.attendance.entities).filter(
    (a) => a.personId === personId,
  );
};

export const {
  toggleAttendance,
  toggleAttendancePaid,
} = attendanceSlice.actions;

export default attendanceSlice.reducer;
