/**
 * Since dates are stored in Redux as ISO strings, they must be parsed to JS Dates
 *   before they can be manipulated with 'date-fns.'
 */

import { format, parseISO } from "date-fns";

// Utilities
import { DATE_FORMAT_NICE } from "@constants";

/**
 * Format an ISO date string
 * @param  {string} dateString   - ISO date string
 * @param  {string} formatString - Date format string
 * @return {string}              - Formatted date string
 */
const formatDateString = (dateString, formatString = DATE_FORMAT_NICE) => {
  if (!dateString) return "";

  try {
    const date = parseISO(dateString);
    return format(date, formatString);
  } catch (e) {
    // NOTE: Fallthrough
    // eslint-disable-next-line no-console
    console.error("Error formatting/parsing date string!", dateString);
  }

  return "";
};

export { formatDateString };
