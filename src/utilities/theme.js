import { DefaultTheme } from "react-native-paper";

export const colors = {
  ...DefaultTheme.colors,
  // Theme colors
  accent: "#eb144c",
  background: "#f6f6f6",
  error: "#c62828",
  primary: "#2196f3",
  primaryDark: "#1976d2",
  primaryLight: "#64b5f6",
  surface: "#ffffff",
  text: "#000000",
  // Named colors
  white: "#ffffff",
  black: "#000000",
  // Shades
  grey: "#757575",
  greyDark: "#424242",
  greyLight: "#bdbdbd",
};

export default {
  ...DefaultTheme,
  colors,
  // Common styles
  styles: {
    list: {
      width: "100%",
    },
    listItemActions: {
      flexDirection: "row",
      alignItems: "center",
    },
  },
};
