import { formatISO } from "date-fns";
import { v4 as uuid4v } from "uuid";

// DEBUG: Can be inserted via Developer Settings
const debugEvents = [
  {
    id: uuid4v(),
    name: "Choir Music",
    date: formatISO(new Date(2020, 9, 10)),
  },
  {
    id: uuid4v(),
    name: "Youth Camping",
    date: formatISO(new Date(2020, 3, 28)),
  },
  {
    id: uuid4v(),
    name: "Friday Baseball",
    date: formatISO(new Date(2020, 6, 4)),
  },
  {
    id: uuid4v(),
    name: "Upcoming Event",
    date: formatISO(new Date(2020, 11, 28)),
  },
  {
    id: uuid4v(),
    name: "Archived Event",
    date: formatISO(new Date(2020, 3, 2)),
    archivedAt: formatISO(new Date()),
  },
];

// DEBUG: Can be inserted via Developer Settings
const debugPeople = [
  { id: uuid4v(), name: "John Klaus", archivedAt: null },
  { id: uuid4v(), name: "Petra Jokavic", archivedAt: null },
  { id: uuid4v(), name: "Lars Ekson", archivedAt: null },
  { id: uuid4v(), name: "Ned Meyers", archivedAt: null },
  { id: uuid4v(), name: "Alexander Seti", archivedAt: null },
  { id: uuid4v(), name: "Tripp Wesley", archivedAt: formatISO(new Date()) },
];

const eIds = debugEvents.map((e) => e.id);
const pIds = debugPeople.map((p) => p.id);
const paidDate = formatISO(new Date());

// DEBUG: Can be inserted via Developer Settings
const debugAttendance = [
  { id: uuid4v(), eventId: eIds[0], personId: pIds[0], paidAt: null },
  { id: uuid4v(), eventId: eIds[0], personId: pIds[1], paidAt: null },
  { id: uuid4v(), eventId: eIds[0], personId: pIds[2], paidAt: paidDate },
  { id: uuid4v(), eventId: eIds[0], personId: pIds[3], paidAt: null },
  { id: uuid4v(), eventId: eIds[0], personId: pIds[4], paidAt: paidDate },
  { id: uuid4v(), eventId: eIds[1], personId: pIds[2], paidAt: paidDate },
  { id: uuid4v(), eventId: eIds[1], personId: pIds[4], paidAt: paidDate },
  { id: uuid4v(), eventId: eIds[1], personId: pIds[1], paidAt: paidDate },
  { id: uuid4v(), eventId: eIds[2], personId: pIds[1], paidAt: paidDate },
  { id: uuid4v(), eventId: eIds[2], personId: pIds[2], paidAt: null },
];

export {
  debugAttendance as DEBUG_attendance,
  debugEvents as DEBUG_events,
  debugPeople as DEBUG_people,
};
